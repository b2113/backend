'use strict';
const { Model } = require('sequelize');

/* Pertama, kita import bcrypt untuk melakukan enkripsi */
const bcrypt = require('bcrypt');

module.exports = (sequelize, DataTypes) => {
	class user_game extends Model {
		/**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
		static associate(models) {
			// define association here
			user_game.hasOne(models.user_game_biodata, { foreignKey: 'id_user_game', as: 'user_biodata' });
		}
		// Method untuk melakukan enkripsi
		static encrypt = (password) => bcrypt.hashSync(password, 10);
		// Lalu, kita buat method register
		static register = ({ username, password, email, isAdmin }) => {
			const encryptedPassword = this.encrypt(
				password
			); /*
        #encrypt dari static method
        encryptedPassword akan sama dengan string
        hasil enkripsi password dari method #encrypt
        */
			return this.create({ username, password: encryptedPassword, email, isAdmin });
		};
	}
	user_game.init(
		{
			username: DataTypes.STRING,
			email: DataTypes.STRING,
			password: DataTypes.STRING,
			isAdmin: DataTypes.BOOLEAN
		},
		{
			sequelize,
			modelName: 'user_game'
		}
	);
	return user_game;
};
