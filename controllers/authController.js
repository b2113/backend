// controllers/authController.js
const { user_game, user_game_biodata } = require('../models');

module.exports = {
	register: (req, res, next) => {
		// Kita panggil static method register yang sudah kita buat tadi
		user_game
			.register(req.body)
			.then((result) => {
				user_game_biodata.create({
					id_user_game: result.id
				});
				res.status(201).json({
					result: 'SUCCESS',
					message: result
				});
			})
			.catch((err) => {
				res.status(500).json({
					result: 'FAILED',
					message: err.message || 'Some error occurred while creating the Player.'
				});
			});
	}
};
